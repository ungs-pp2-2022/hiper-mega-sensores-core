package good.posture.sensors.core.notify;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicInteger;

import good.posture.sensors.core.Range;
import good.posture.sensors.core.persist.BodyPosture;
import good.posture.sensors.core.persist.Persist;

@SuppressWarnings("deprecation")
public class NotifyTelegram extends Notify {

	private static final String BOT_TOKEN = "5611692396:AAE8V-dQLcHabBS9JW9wNXeK7eXBXnEGUO0";
	private static final String CHAT_ID = "1578739651";

	public NotifyTelegram(Persist persist, Map<String, Range> ranges) {
		super(persist, ranges);
	}

	@Override
	public void update(Observable o, Object arg) {
		BodyPosture bodyPosture = (BodyPosture) arg;
		List<BodyPosture> bodyPostures = this.persist.findAll();
		List<BodyPosture> posturesLast10 = bodyPostures.subList(Math.max(bodyPostures.size() - 10, 0),
				bodyPostures.size());

		AtomicInteger count = new AtomicInteger(0);
		StringBuilder message = new StringBuilder(
				"Sus ultimas 10 posturas erroneas de " + bodyPosture.getBodyPart() + " fueron: ");
		posturesLast10.forEach(posture -> {
			if (bodyPosture.getBodyPart().equals(posture.getBodyPart())
					&& ranges.get(posture.getBodyPart()).contains(posture.getValue())) {
				message.append("[ ").append(posture.getValue()).append(" ] ");
				count.incrementAndGet();
			}
		});

		if (count.get() == 10) {
			sendNotify(message.toString());
		}
	}

	private void sendNotify(String message) {
		URL obj;
		try {
			obj = new URL("https://api.telegram.org/bot" + BOT_TOKEN + "/sendMessage?chat_id=" + CHAT_ID + "&text="
					+ message);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			System.out.println("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				System.out.println(response.toString());
			} else {
				System.out.println("GET request not worked");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
