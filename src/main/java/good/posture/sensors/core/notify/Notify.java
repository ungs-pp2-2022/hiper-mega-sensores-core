package good.posture.sensors.core.notify;

import java.util.Map;
import java.util.Observer;

import good.posture.sensors.core.Range;
import good.posture.sensors.core.persist.Persist;

@SuppressWarnings("deprecation")
public abstract class Notify implements Observer {

	protected Persist persist;
	protected Map<String, Range> ranges;
	
	public Notify(Persist persist, Map<String, Range> ranges) {
		this.persist = persist;
	}
	
}
