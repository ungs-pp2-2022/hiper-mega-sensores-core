package good.posture.sensors.core.notify;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import good.posture.sensors.core.Range;
import good.posture.sensors.core.persist.BodyPosture;
import good.posture.sensors.core.persist.Persist;

@SuppressWarnings("deprecation")
public class NotifySound extends Notify {

	public NotifySound(Persist persist, Map<String, Range> ranges) {
		super(persist, ranges);
	}

	@Override
	public void update(Observable o, Object arg) {
		BodyPosture bodyPosture = (BodyPosture) arg;
		List<BodyPosture> bodyPostures = this.persist.findAll();
		List<BodyPosture> posturesLast10 = bodyPostures.subList(Math.max(bodyPostures.size() - 10, 0),
				bodyPostures.size());

		AtomicInteger count = new AtomicInteger(0);
		
		posturesLast10.forEach(posture -> {
			if (bodyPosture.getBodyPart().equals(posture.getBodyPart())
					&& ranges.get(posture.getBodyPart()).contains(posture.getValue())) {
				count.incrementAndGet();
			}
		});

		if (count.get() == 10) {
			sendNotify();
		}
	}

	private void sendNotify() {
		File file = new File("error-sound.wav");
		AudioInputStream audioStream;
		try {
			audioStream = AudioSystem.getAudioInputStream(file);
			Clip clip = AudioSystem.getClip();
			clip.open(audioStream);
			clip.start();
			TimeUnit.SECONDS.sleep(1);
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
