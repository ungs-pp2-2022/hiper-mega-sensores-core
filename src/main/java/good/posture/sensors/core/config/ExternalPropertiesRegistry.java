package good.posture.sensors.core.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ExternalPropertiesRegistry {

	private static Properties soleInstance;
	
	private static final String PROPERTIES_PATH = "configuration.properties";
	
	private static final String BODY_PART = ".body.part";
	
	private static final String BODY_MINRANGE = ".body.minrange";
	
	private static final String BODY_MAXRANGE = ".body.maxrange";
	
	private static final String DIRECTORY_DISCOVERY = "directory.discovery";
	
	private static Properties getInstance() {
		if (soleInstance == null) {
			soleInstance = new Properties();
			try {
				soleInstance.load(new FileInputStream(PROPERTIES_PATH));
			} 
			catch (IOException ex) {
			    ex.printStackTrace();
			}
		}
		return soleInstance;
	}
	
	
	public static String getBodyPart(String prop) {
		return getInstance().getProperty(prop + BODY_PART) ;
	}
	
	public static Integer getBodyMinRange(String prop) {
		return Integer.parseInt(getInstance().getProperty(prop + BODY_MINRANGE));
	}
	
	public static Integer getBodyMaxRange(String prop) {
		return Integer.parseInt(getInstance().getProperty(prop + BODY_MAXRANGE));
	}
	
	public static String getBodyDirectoryDiscovery() {
		return getInstance().getProperty(DIRECTORY_DISCOVERY);
	}
	
}
