package good.posture.sensors.core.config;

import java.io.IOException;
import java.util.Properties;

public class PropertiesRegistry {

	private static Properties soleInstance;
	
	private static final String PROPERTIES_PATH = "config.properties";
	
	private static final String DISCOVERY_CLASS_PROP = "discoveryClass";

	private static Properties getInstance() {
		if (soleInstance == null) {
			soleInstance = new Properties();
			try {
				soleInstance.load(PropertiesRegistry.class.getClassLoader().getResourceAsStream(PROPERTIES_PATH));
			} 
			catch (IOException ex) {
			    ex.printStackTrace();
			}
		}
		return soleInstance;
	}
	
	public static String getDiscoveryClassName() {
		return getInstance().getProperty(DISCOVERY_CLASS_PROP);
	}

}
