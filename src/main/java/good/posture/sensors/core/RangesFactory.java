package good.posture.sensors.core;

import java.util.HashMap;
import java.util.Map;

import good.posture.sensors.core.config.ExternalPropertiesRegistry;

public class RangesFactory {

	private static final String DEFAULT = "default";
	private static final String FOOT_MALE = "foot_male";
	private static final String FOOT_FEMALE = "foot_female";

	
	public Map<String, Range> getRanges(String[] args) {
		Map<String, Range> ranges = new HashMap<>();
		if (args.length == 0) {
			ranges.put(ExternalPropertiesRegistry.getBodyPart(DEFAULT),
					new Range(ExternalPropertiesRegistry.getBodyMinRange(DEFAULT),
							ExternalPropertiesRegistry.getBodyMaxRange(DEFAULT)));
		} else {
			String command = args[0];
			
			
			switch (command) {

			case "foot":
				ranges.put(ExternalPropertiesRegistry.getBodyPart(DEFAULT),
						new Range(ExternalPropertiesRegistry.getBodyMinRange(DEFAULT),
								ExternalPropertiesRegistry.getBodyMaxRange(DEFAULT)));
				ranges.put(ExternalPropertiesRegistry.getBodyPart(FOOT_MALE),
						new Range(ExternalPropertiesRegistry.getBodyMinRange(FOOT_MALE),
								ExternalPropertiesRegistry.getBodyMaxRange(FOOT_MALE)));
				ranges.put(ExternalPropertiesRegistry.getBodyPart(FOOT_FEMALE),
						new Range(ExternalPropertiesRegistry.getBodyMinRange(FOOT_FEMALE),
								ExternalPropertiesRegistry.getBodyMaxRange(FOOT_FEMALE)));
				break;

				
			case "test1":
				ranges.put("FOOT", new Range(80, 120));
				break;

			default:
				ranges.put(ExternalPropertiesRegistry.getBodyPart(DEFAULT),
						new Range(ExternalPropertiesRegistry.getBodyMinRange(DEFAULT),
								ExternalPropertiesRegistry.getBodyMaxRange(DEFAULT)));
			}
		}

		return ranges;
	}
}
