package good.posture.sensors.core;

import java.util.Map;
import java.util.Observer;
import java.util.Optional;
import java.util.Set;

import good.posture.sensors.core.config.ExternalPropertiesRegistry;
import good.posture.sensors.core.discovery.DiscoveryClass;
import good.posture.sensors.core.discovery.DiscoveryClassFactory;
import good.posture.sensors.core.notify.Notify;
import good.posture.sensors.core.notify.NotifySound;
import good.posture.sensors.core.notify.NotifyTelegram;
import good.posture.sensors.core.persist.Persist;
import good.posture.sensors.core.persist.PersistInMemory;

@SuppressWarnings("deprecation")
public class BodyPostureInitializer {

	private String[] args;

	private static final String GOOD_POSTURE_SENSORS_EXTENSIBLE_HISTORIAL = "good.posture.sensors.extensible.observers.";

	public BodyPostureInitializer(String[] args) {
		this.args = args;
	}

	public BodyPostureCore initialize() {
		
		RangesFactory rangesFactory = new RangesFactory();
		Map<String, Range> ranges = rangesFactory.getRanges(args);
		
		Persist persist = new PersistInMemory();
		BodyPostureCore core = new BodyPostureCore(ranges, persist);
		
		DiscoveryClassFactory discoveryFactory = new DiscoveryClassFactory();
		
		DiscoveryClass classDiscovery = discoveryFactory.getInstance();
		
		Set<Class> classes = classDiscovery.findAllClasses(ExternalPropertiesRegistry.getBodyDirectoryDiscovery(),
				GOOD_POSTURE_SENSORS_EXTENSIBLE_HISTORIAL);
		Optional<Class> classOpt = classes.stream().findFirst();
		if (classOpt.isPresent()) {
			try {
				Observer observer = (Observer) classOpt.get().newInstance();
				core.addObserver(observer);
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		Notify notifyTelegram = new NotifyTelegram(persist, ranges);
		core.addObserver(notifyTelegram);
		
		Notify notifySound = new NotifySound(persist, ranges);
		core.addObserver(notifySound);
		return core;
	}
}
