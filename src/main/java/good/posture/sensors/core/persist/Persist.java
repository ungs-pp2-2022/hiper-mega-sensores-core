package good.posture.sensors.core.persist;

import java.util.List;

public interface Persist{

	void persist(BodyPosture bodyPosture);

	List<BodyPosture> findAll();
}
