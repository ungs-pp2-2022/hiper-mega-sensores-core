package good.posture.sensors.core.persist;

import java.util.ArrayList;
import java.util.List;

public class PersistInMemory implements Persist {

	private List<BodyPosture> bodyPostures = new ArrayList<>();
	
	@Override
	public void persist(BodyPosture bodyPosture) {
		if(bodyPosture == null) {
			throw new IllegalArgumentException("La postura no debe ser nula");
		}
		bodyPostures.add(bodyPosture);
	}

	@Override
	public List<BodyPosture> findAll() {
		return bodyPostures;
	}


}
