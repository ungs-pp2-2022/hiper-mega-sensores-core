package good.posture.sensors.core.persist;

public class BodyPosture {

	private String bodyPart;

	private Integer value;

	public BodyPosture(String bodyPart, Integer value) {
		this.bodyPart = bodyPart;
		this.value = value;
	}

	public String getBodyPart() {
		return bodyPart;
	}

	public void setBodyPart(String bodyPart) {
		this.bodyPart = bodyPart;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
