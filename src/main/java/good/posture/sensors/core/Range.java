package good.posture.sensors.core;

public class Range {

	private Integer min;
	private Integer max;
	
	public Range(Integer min, Integer max) {
		this.min = min;
		this.max = max;
	}
	
	public boolean contains(Integer value) {
		return value >= this.min && value <= this.max;
	}
}
