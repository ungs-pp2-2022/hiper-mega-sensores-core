package good.posture.sensors.core;

import java.util.List;
import java.util.Map;
import java.util.Observable;

import good.posture.sensors.core.persist.Persist;
import good.posture.sensors.extensible.observers.entity.BodyPosture;


@SuppressWarnings("deprecation")
public class BodyPostureCore extends Observable {

	private Map<String, Range> bodyPostureRanges;
	private List<String> alertChannels;
	private String SOUND_ALERT_CHANNEL = "sound";
	private String TELEGRAM_ALERT_CHANNEL = "telegram";
	private Persist persist;

	public BodyPostureCore(Map<String, Range> bodyPostureRangeValidators, Persist persist) {
		this.bodyPostureRanges = bodyPostureRangeValidators;
		this.persist = persist;
	}
	
	public boolean checkPosture(String bodyPart, Integer postureValue) {
		
		if(!bodyPostureRanges.containsKey(bodyPart)) {
			throw new IllegalArgumentException("La parte del cuerpo " + bodyPart + " no est� definida.");
		}
		if(bodyPart == null || postureValue == null) {
			throw new IllegalArgumentException("Los valores de parte del cuerpo y de postura no pueden ser nulos");
		}
		BodyPosture bodyPosture = new BodyPosture(bodyPart, postureValue);
		persist.persist(new good.posture.sensors.core.persist.BodyPosture(bodyPart, postureValue));
		setChanged();
		notifyObservers(bodyPosture);
		return bodyPostureRanges.get(bodyPart).contains(postureValue);
	}
	
	public void activateAlertChannel(String alertChannel) {
		this.validateAlertChannel(alertChannel);
		alertChannels.add(alertChannel);
	}
	
	public void deactivateAlertChannel(String alertChannel) {
		this.validateAlertChannel(alertChannel);
		alertChannels.remove(alertChannel);
	}
	
	private void validateAlertChannel(String alertChannel) {
		if (alertChannel != this.SOUND_ALERT_CHANNEL && alertChannel != this.TELEGRAM_ALERT_CHANNEL) {
			throw new IllegalArgumentException("El canal de alertas: " + alertChannel + " no es valido");
		}
	}
}
