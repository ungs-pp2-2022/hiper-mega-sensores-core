package good.posture.sensors.core.discovery;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

public class DiscoveryClassImpl implements DiscoveryClass {

	@Override
	public Set<Class> findAllClasses(String discoveryPath, String pathClass) {
		Set<Class> classes = new HashSet<>();
		File dir = new File(discoveryPath);
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File child : directoryListing) {
				String fileName = child.getName();
				if (fileName.endsWith(".class")) {
					String classname = fileName.replace('/', '.').substring(0, fileName.length() - 6);
					try {
						URL[] cp = { child.toURI().toURL() };
						URLClassLoader urlcl = new URLClassLoader(cp);
						Class clazz = urlcl.loadClass(pathClass + classname);
						classes.add(clazz);
					} catch (Throwable e) {
						System.out.println("WARNING: failed to instantiate " + classname + " from " + fileName);
					}
				}
			}
		}
		return classes;
	}
}
