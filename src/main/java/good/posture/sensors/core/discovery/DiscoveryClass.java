package good.posture.sensors.core.discovery;

import java.util.Set;

public interface DiscoveryClass {

	public Set<Class> findAllClasses(String discoveryPath, String pathClass);
}
