package good.posture.sensors.core.discovery;

import good.posture.sensors.core.config.PropertiesRegistry;

public class DiscoveryClassFactory {

	public DiscoveryClass getInstance() {
		try {
			return (DiscoveryClass) Class.forName(PropertiesRegistry.getDiscoveryClassName()).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
