package good.posture.sensors.core.discovery;

import java.util.HashSet;
import java.util.Set;

import good.posture.sensors.extensible.observers.BodyPostureObserver;

public class DiscoveryClassMock implements DiscoveryClass {

	private static final String GOOD_POSTURE_SENSORS_EXTENSIBLE_HISTORIAL_MOCK = "good.posture.sensors.extensible.observers.";
	
	private static final String DISCOVERY_PATH_MOCK = "/discoveryClass";
	@Override
	public Set<Class> findAllClasses(String discoveryPath, String pathClass) {
		Set<Class> classSet = new HashSet<>();
		if(discoveryPath.equals(DISCOVERY_PATH_MOCK) && pathClass.equals(GOOD_POSTURE_SENSORS_EXTENSIBLE_HISTORIAL_MOCK)) {
			classSet.add(BodyPostureObserver.class);
		}
		return classSet;
	}

}
